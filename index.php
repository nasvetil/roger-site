<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="normalize.min.css">
	<title>Site!</title>
</head>
<body>
	<div class="page-container">
		<div class="page-container__wrap">
			<div class="auth">
				<div class="auth__wrap">
					<form method="POST" action="auth.php">
						<div class="auth__item">
							<label class="auth__title" for="nikname" class="auth__item">Логин:</label>
							<input class="auth__input" type="text" name="nikname" id="nikname">
						</div>
						<div class="auth__item">
							<label class="auth__title" for="login" class="auth__item">Пароль:</label>
							<input class="auth__input" type="password" name="login" id="login">
						</div>
						<input class="auth__button" type="submit" value="Войти">
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
